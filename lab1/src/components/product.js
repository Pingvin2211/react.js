import React from 'react';

const Product = () => {
    return (
        <div style={{border: "1px solid black", width: 150,float: 'left', margin: '5px', borderRadius: 5}}>
            <img width={150} height={150} src="https://www.firstbenefits.org/wp-content/uploads/2017/10/placeholder.png" alt=""/>
            <h4 style={{display: "inline"}}>title</h4>
            <p style={{display: ""}}>1000 uah</p>
            <button style={{width: "100%", borderRadius: 0, border: 0, padding: "5px 0px"}}>Purchase</button>
        </div>
    );
};

export default Product;