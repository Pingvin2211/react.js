import React, {useState} from 'react';

const Product2 = (props) => {
    const {textColor, text} = props
    const [color] = useState('red')

    const style = {
        backgroundColor: color,
        color: textColor
    }
    return (
        <div style={style}>
            I'm {color} and content {text}
        </div>
    );
};

export default Product2;