import './App.css';
import Task1 from "./tasks/task1";
import Task2 from "./tasks/task2";
import Task3 from "./tasks/task3";
import Task4 from "./tasks/task4";
import Task5 from "./tasks/task5";
import Task6 from "./tasks/task6";
import Task7 from "./tasks/task7";

function App() {
  return (
    <div className="App">
      <Task7/>
    </div>
  );
}

export default App;
