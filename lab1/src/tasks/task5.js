import React, {useEffect, useState} from 'react';

const Task5 = () => {
    const cities = [
        {id: 1, name: "Chicago", image: 'chicago.jpg'},
        {id: 2, name: "Los Angeles", image: 'los-angeles.jpg'},
        {id: 3, name: "New York", image: 'new-york.jpg'},
    ]
    const [currentCityImage, setCurrentCityImage] = useState(cities[0].image)
    return (
        <div>
            <select onChange={e => setCurrentCityImage(e.currentTarget.value)} style={{padding: 10}}>
                {cities.map(item => (
                    <option key={item.id} value={item.image}>{item.name}</option>
                ))}
            </select>
            <br/>
            <img width={500} src={"/images/" + currentCityImage} alt=""/>
        </div>
    );
};

export default Task5;