import React from 'react';

const Task2 = () => {
    return (
        <div>
            <table cellSpacing={"0px"} cellPadding={"5px"} border={1}>
                <tr>
                    <td><b>First Name</b></td>
                    <td>John</td>
                </tr>
                <tr>
                    <td><b>NameLast</b></td>
                    <td>Silver</td>
                </tr>
                <tr>
                    <td><b>Occupation</b></td>
                    <td>Pirate Captain</td>
                </tr>
            </table>
        </div>
    );
};

export default Task2;