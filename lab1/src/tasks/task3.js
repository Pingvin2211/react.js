import React from 'react';

const Task3 = () => {
    const product1 = {
        name: 'Mouse',
        color: 'black'
    }
    const visible = true
    return (
        <>
            <Product isVisible={visible} product={product1}/>
        </>
    );
};

export default Task3;

const Product = (props) => {
    return (
        <div style={{display: props.isVisible ? "block" : "none"}}>
            I'm a {props.product.color} {props.product.name}
        </div>
    );
};