import React, {useState} from 'react';

const Task7 = () => {
    const [color, setColor] = useState("black")
    return (
        <div>
            <h1>I am a {color} Product!</h1>
            <select onChange={e => setColor(e.currentTarget.value)}>
                <option value="black">black</option>
                <option value="red">red</option>
                <option value="yellow">yellow</option>
            </select>
        </div>
    );
};

export default Task7;