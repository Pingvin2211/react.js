import React from 'react';
import {List} from "react-virtualized";
import Image from "./image";

const Table = (props) => {
    const {list} = props
    function renderRow({index, key, style}) {
        let text = list[index].title
        //if (text.split(" ").length<=7) {
            let photo = list[index]
            return <Item key={key} style={style} text={text} photo={photo}/>
       //}
        //else return 0
    }
    return (
        <div>
            <div className="list">
                <List filter
                    width={1000}
                    height={800}
                    rowHeight={150}
                    rowRenderer={renderRow}
                    rowCount={list.length}
                    overscanRowCount={1}/>
            </div>
        </div>
    );
};
const Item = (props) => {
    const {key, style, text, photo} = props;
    //let sliced = text.slice(0, 7);
    //if (sliced.length < text.length) {
    //    sliced += '...';
    //}
    return (
        <div key={key} style={style} className="row">
            <Image photo={photo}/>
            <div className="content">
                <div>{text}</div>
            </div>
        </div>
    )
}

export default Table;