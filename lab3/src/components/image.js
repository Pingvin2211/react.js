import React from 'react';

const Image = (props) => {
    const {photo} = props
    return (
        <div>
            <div className="image">
                <a href={photo.url}>
                    <img src={photo.thumbnailUrl} alt=""/>
                </a>
            </div>
        </div>
    );
};

export default Image;