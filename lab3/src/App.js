import './App.css';
import Table from "./components/table";
import {useEffect, useState} from "react";

function App() {
    const [list,setList] = useState([])
    const getList = async () => {
        const response = await fetch(`https://jsonplaceholder.typicode.com/photos?albumId=1`);
        const data = response.json();
        return data;
    }
    useEffect(() => {
        getList().then(data => {
            data = data.filter((item) => { 
                return item.title.split(" ").length <= 7;
             });
            setList(data);
        })
    }, [ ])
  return (
    <div className={'App'}>
        <Table list={list}/>
    </div>
  );
}

export default App;
