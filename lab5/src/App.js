
import './App.css';
import Task01 from './components/Task01/Task01';
import Notification from "./Notification";

function App() {
  return (
    <div className="App">
      <Task01/>
      <Notification/>
    </div>
  );
}

export default App;
