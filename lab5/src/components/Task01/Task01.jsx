import {AiOutlineComment} from "react-icons/ai";
import {useState} from "react";

import styles from './task01.module.scss';

const Task01 = () => {
  
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [theme, setTheme] = useState('');
  const [comment, setComment] = useState('');
  
  const [emailError, setEmailError] = useState('');
  const [themeError, setThemeError] = useState('');
  const [formError, setFormError] = useState('');
  
  const emailHandler = (e) => {
    const value = e.target.value;
    setEmail(value);
    
    if (!value) {
      setEmailError('Поле обов\'язкове.');
      return;
    }
    
    const pattern = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/;
    setEmailError(!value.match(pattern) ? 'Неправильна адреса e-mail.' : '');
  }
  
  const themeHandler = (e) => {
    const value = e.target.value;
    setTheme(value);
  
    setThemeError(!value? 'Поле обов\'язкове.' : '');
  }
  
  const formHandler = (e) => {
    e.preventDefault();
    
    if (emailError || themeError || email === '' || theme === '') {
      if (email === '') {
        setEmailError('Поле обов\'язкове.');
      }
      
      if (theme === '') {
        setThemeError('Поле обов\'язкове.' );
      }
      
      setFormError('Одне або декілька полів містять помилкові дані.');
      return;
    }
    
    const request = {name, email, theme, comment};
    console.log('Request', request);
    
    setName('');
    setTheme('');
    setComment('');
    setEmail('');
    setFormError('');
  }
  
  return (
    <div>
      <div className={styles.titleContainer}>
        <AiOutlineComment size={32}/>
        <h3 className={styles.title}>Зворотній зв'язок</h3>
      </div>
      <div className={styles.container}>
        <p>Задай своє запитання, або повідом про порушення під час вступної кампанії</p>
      </div>
      
      <div className={styles.container}>
        <form onSubmit={formHandler} className={styles.form}>
          <div className={styles.inputContainer}>
            <input value={name} onChange={(e) => setName(e.target.value)} className={styles.input} type="text" placeholder="Ім'я"/>
          </div>
          
          <div className={styles.inputContainer}>
            <input value={email} onChange={emailHandler} className={styles.input} type="email" placeholder="E-mail*"/>
            <p className={styles.error}>{emailError}</p>
          </div>
          
          <div className={styles.inputContainer}>
            <input value={theme} onChange={themeHandler} className={styles.input} type="text" placeholder="Тема*"/>
            <p className={styles.error}>{themeError}</p>
          </div>
          
          <textarea value={comment} onChange={(e) => setComment(e.target.value)} className={styles.textarea} placeholder="Коментар"/>
          
          <p className={styles.mgLeft} style={{color: 'gray', fontSize: '11px'}}>Поля відмічені * мають бути обов'язково заповненими</p>
          
          <button className={styles.button} type="submit">Відправити</button>
          
          <p className={styles.mgLeft} style={{color: 'red'}}>{formError}</p>
        </form>
      </div>
    </div>
  );
};

export default Task01;
