import React, {useEffect, useState} from 'react';

const MyCounter = (props) => {
    const {maxCount, minCount, initial, id} = props.counter
    const [count, setCount] = useState(initial ?? 0)
    const increaseHandler = () => {
            if (count < (maxCount !== undefined ? maxCount : 10))
                setCount(count + 1)
    }
    const decreaseHandler = () => {
        if (count > (minCount !== undefined ? minCount : -10))
            setCount(count - 1)
    }
    const clearHandler = () => {
        setCount(initial??0)
    }
    return (
        <div className={'d-flex align-items-center m-3'}>
            Поточний рахунок: {count}
            <button onClick={increaseHandler} className={"btn btn-outline-primary ms-2"}>+</button>
            <button onClick={decreaseHandler} className={"btn btn-outline-primary mx-2"}>-</button>
            <button onClick={clearHandler} className={"btn btn-outline-primary"}>Очистити</button>
        </div>
    );
};

export default MyCounter;