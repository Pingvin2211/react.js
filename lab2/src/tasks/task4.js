import React, {useEffect, useState} from 'react';

const Task4 = () => {
    const [isGameOver, setGameOver] = useState(true)
    const [number, setNumber] = useState(0)
    const [estimatedNumber, setEstimatedNumber] = useState()
    const [attempts, setAttempts] = useState(0)
    const [message, setMessage] = useState("")
    const maxNumber = 1000
    const [information, setInformation] = useState([])
    const startGameHandler = () => {
        setGameOver(false)
        setNumber(Math.floor(Math.random()*maxNumber))
        setAttempts(10)
        setMessage("")
        setInformation([])
    }
    const checkHandler = () => {
        if (estimatedNumber == number){
            setGameOver(true)
            setMessage("You won!")
            setAttempts(0)
        }
        if (estimatedNumber != number){
            let arrow = estimatedNumber > number ? "<" : ">"
            let ft = [`N ${arrow} ${estimatedNumber}`]
            if (information.length < 1) {
                setInformation(ft)
            }else{
                setInformation([...information, ...ft])
            }
        }
        setAttempts(attempts-1)
        if (attempts <= 1 && estimatedNumber != number){
            setGameOver(true)
            setMessage("Game Over!You lost((")
        }
    }
    return (
        <div className={'col-2 m-3'}>
            <div className={'input-group'}>
                <button onClick={startGameHandler} disabled={!isGameOver} className={'btn btn-outline-primary d-inline'}>New game</button>
                <input onInput={e => setEstimatedNumber(e.currentTarget.value !== "" ? e.currentTarget.value : undefined)} className={'form-control d-inline'} type="number"/>
                <button onClick={checkHandler} disabled={estimatedNumber === undefined || isGameOver} className={'btn btn-primary d-inline'}>Check</button>
            </div>
            <h5 className={'mt-3'}>Information:</h5>
            <div style={{minHeight: "120px"}} className={'bg-warning border border-1 border-dark rounded px-2'}>
                {information.map(item =>
                    <>
                        <p className={"d-inline"}>{item}</p>
                        <br/>
                    </>
                )}
            </div>
            <h6>Attempts:{attempts}</h6>
            <h4>Result:{message}</h4>
        </div>
    );
};

export default Task4;