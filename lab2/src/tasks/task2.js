import React from 'react';
import MyCounter from "../components/MyCounter";

const Task2 = () => {
    const counters = [
        {id: 1, initial: 6, minCount: -5, maxCount: 10},
        {id: 2, initial: 0},
        {id: 3},
    ]
    return (
        <div>
            {counters.map(item => (
                <MyCounter counter={item}/>
            ))}
        </div>
    );
};

export default Task2;