import React, {useEffect, useState} from 'react';


const Task3 = () => {
    const [products, setProducts] = useState([
        {id: 1, name: "Constructor Lego", price: 300, min: 0, max: 10, count: 0},
        {id: 2, name: "Train Station", price: 200, min: 0, max: 10, count: 0},
        {id: 3, name: "Hot Wheels Track", price: 150, min: 0, max: 20, count: 0},
    ])
    const plusHandler = (id, maxVal) => {
        setProducts(products.map(item => {
            if (item.id === id && item.count < maxVal){
                item.count += 1
            }
            return item
        }))
    }
    const minusHandler = (id, minVal) => {
        setProducts(products.map(item => {
            if (item.id === id && item.count > minVal){
                item.count -= 1
            }
            return item
        }))
    }
    const [totalPrice, setTotalPrice] = useState(0)
    const [quantity, setQuantity] = useState(0)
    useEffect(() => {
        let totalPriceTemp = 0
        let quantityTemp = 0
        products.forEach(item => {
            totalPriceTemp += item.count * item.price
            quantityTemp += item.count
        })
        setTotalPrice(totalPriceTemp)
        setQuantity(quantityTemp)
    }, [products])
    return (
        <div>
            <table className={"table table-bordered"}>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total Price</th>
                </tr>
                </thead>
                <tbody>
                {products.map(item =>
                    <tr>
                        <td>{item.name}</td>
                        <td>{item.price}</td>
                        <td>
                            <button className={'btn btn-outline-primary px-2 py-1'} onClick={() => plusHandler(item.id, item.max)}>+</button>
                            <p style={{display: "inline",margin: "5px"}}>{item.count}</p>
                            <button className={'btn btn-outline-primary px-2 py-1'} onClick={() => minusHandler(item.id, item.min)}>-</button>
                        </td>
                        <td>{item.price * item.count}</td>
                    </tr>
                )}
                <tr>
                    <td colSpan={2}>Totals</td>
                    <td>{quantity}</td>
                    <td>{totalPrice}</td>
                </tr>
                </tbody>
            </table>
        </div>
    );
};

export default Task3;