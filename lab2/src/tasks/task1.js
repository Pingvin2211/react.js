import React from 'react';
import MyCounter from "../components/MyCounter";

const Task1 = () => {
    const counter = {id: 1, initial: 5, minCount: -10, maxCount: 10}
    return (
        <div>
            <MyCounter counter={counter}/>
        </div>
    );
};

export default Task1;